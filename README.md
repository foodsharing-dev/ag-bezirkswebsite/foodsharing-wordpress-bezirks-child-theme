# foodsharing Bezirks-Child-Theme

Maintainer (Projekt-Verantwortlicher): Christian Walgenbach<br />
Kontakt unter c.walgenbach@fslubu.de

Das Child-Theme ist nur bei eigenen Anpassungen von CSS- und PHP-Dateien nötig.

Die aktuelle Demo-Seite ist unter 
https://bezirks-child-theme.fslubu.de zu finden

## Installation des Theme:
* Download vom Theme unter https://cloud.fslubu.de/index.php/s/EnMoEAD4SoxeJCS (Version 0.0.2)
* Im Admin-Bereich unter siteurl.topleveldomain/wp-admin unter Design / Themes oben links den Button Hinzufügen und danach Theme hochladen wählen.
* Die Datei fs-bezirks-child-theme.zip hochladen.
* Aktiviere das Child-Theme fs-bezirks-child-theme